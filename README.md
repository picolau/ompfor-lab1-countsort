# OpenMP -for- Labs

Let's learn parallel programming using OpenMP!

## Countsort

Counting sort is an algorithm for sorting a collection of objects according to keys that are small integers.

See [Wikipedia](https://en.wikipedia.org/wiki/Counting_sort).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor.
4. Compile the program using CMake and run it:

        cd <working_dir>/ompfor-lab1-countsort
        mkdir build
        cd build
        cmake -DCMAKE_BUILD_TYPE=Release ..
        make
        ./countsort-serial < ../inputs/arq1.in

4. Make a copy of the code (call it 'countsort-parallel.c' for example).
5. Parallelize the code using OpenMP (add pragmas).
6. Add it to CMakeLists.txt in order to build both versions.
7. Compare performance between serial and parallel version. Determine the speedup?


Anything missing? Ideas for improvements? Make a pull request.
